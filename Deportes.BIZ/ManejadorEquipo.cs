﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Deportes.COMMON.Entidades;
using Deportes.COMMON.Interfaces;
using MongoDB.Bson;

namespace Deportes.BIZ
{
    public class ManejadorEquipo : IManejadorEquipos
    {
        IRepositorio<Equipo> repositorioEquipo;
        public ManejadorEquipo(IRepositorio<Equipo> repositorioEqu)
        {
            repositorioEquipo = repositorioEqu;
        }
        public List<Equipo> Listar => repositorioEquipo.Read.OrderBy(e => e.NombreEqu).ToList();

        public bool Agregar(Equipo entidad)
        {
            return repositorioEquipo.Create(entidad);
        }

        public Equipo BuscarPorId(ObjectId id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(ObjectId id)
        {
            return repositorioEquipo.Delete(id);
        }

        public List<Equipo> EquiposPorDeporte(string equipo)
        {
            return Listar.Where(e => e.NombreEqu == equipo).ToList();
        }

        public bool Modificar(Equipo entidad)
        {
            return repositorioEquipo.Update(entidad);
        }
    }
}
