﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Deportes.COMMON.Entidades;
using Deportes.COMMON.Interfaces;
using MongoDB.Bson;

namespace Deportes.BIZ
{
    public class ManejadorDeporte : IManejadorDeportes
    {
        IRepositorio<Deporte> repositorioDeporte;
        public ManejadorDeporte(IRepositorio<Deporte> repositorioDepor)
        {
            repositorioDeporte = repositorioDepor;
        }
        public List<Deporte> Listar => repositorioDeporte.Read.OrderBy(p => p.NombreDep).ToList();

        public bool Agregar(Deporte entidad)
        {
            return repositorioDeporte.Create(entidad);
        }

        public Deporte BuscarPorId(ObjectId id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(ObjectId id)
        {
            return repositorioDeporte.Delete(id);
        }


        public bool Modificar(Deporte entidad)
        {
            return repositorioDeporte.Update(entidad);
        }
    }
}
