﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Deportes.COMMON.Entidades;
using Deportes.COMMON.Interfaces;
using MongoDB.Bson;

namespace Deportes.BIZ
{
    public class ManejadorTorneos : IManejadorTorneo
    {
        IRepositorio<Torneo> repositorioTorneo;
        public ManejadorTorneos(IRepositorio<Torneo> repositorioTor)
        {
            repositorioTorneo = repositorioTor;
        }
        public List<Torneo> Listar => repositorioTorneo.Read.OrderBy(e => e.NombreTorn).ToList();

        public bool Agregar(Torneo entidad)
        {
            return repositorioTorneo.Create(entidad);
        }

        public Torneo BuscarPorId(ObjectId id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(ObjectId id)
        {
            return repositorioTorneo.Delete(id);
        }

        public bool Modificar(Torneo entidad)
        {
            return repositorioTorneo.Update(entidad);
        }

        public List<Torneo> TorneosPorDeporte(string torneo)
        {
            return Listar.Where(e => e.NombreTorn == torneo).ToList();
        }
    }
}
