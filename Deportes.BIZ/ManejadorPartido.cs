﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Deportes.COMMON.Entidades;
using Deportes.COMMON.Interfaces;
using MongoDB.Bson;

namespace Deportes.BIZ
{
    public class ManejadorPartido : IManejadorPartido
    {
        IRepositorio<Partido> repositorioPartido;
        public ManejadorPartido(IRepositorio<Partido> repoPartido)
        {
            repositorioPartido = repoPartido;
        }
        public List<Partido> Listar => repositorioPartido.Read;

        public bool Agregar(Partido entidad)
        {
            return repositorioPartido.Create(entidad);
        }

        public Partido BuscarPorId(ObjectId id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(ObjectId id)
        {
            return repositorioPartido.Delete(id);
        }

        public bool Modificar(Partido entidad)
        {
            return repositorioPartido.Update(entidad);
        }
    }
}
