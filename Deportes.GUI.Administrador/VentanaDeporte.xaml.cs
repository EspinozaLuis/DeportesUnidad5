﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Deportes.BIZ;
using Deportes.COMMON.Entidades;
using Deportes.COMMON.Interfaces;
using Deportes.DAL;

namespace Deportes.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para VentanaDeporte.xaml
    /// </summary>
    public partial class VentanaDeporte : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }
        IManejadorDeportes manejadorDeportes;
        
        
        accion accionDeportes;
        
        

        public VentanaDeporte()
        {
            InitializeComponent();
            manejadorDeportes = new ManejadorDeporte(new RepositorioGenerico<Deporte>());
            LimpiarCampos();
            BotonesEnEdicionDeportes(false);
            ActualizarTablaDeportes();
            TexboxEdicion(false);
        }

        private void ActualizarTablaDeportes()
        {
            dtgDeportes.ItemsSource = null;
            dtgDeportes.ItemsSource = manejadorDeportes.Listar;
        }

        private void TexboxEdicion(bool value)
        {
            txbNombreDepor.IsEnabled = value;
        }

        private void BotonesEnEdicionDeportes(bool value)
        {
            btnCancelarDepor.IsEnabled = value;
            btnEditarDepor.IsEnabled = !value;
            btnEliminarDepor.IsEnabled = !value;
            btnGuardarDepor.IsEnabled = value;
            btnNuevoDepor.IsEnabled = !value;
        }

        private void LimpiarCampos()
        {
            txbNombreDepor.Clear();
            txbIdDeporte.Text = "";
        }

        private void btnNuevoDepor_Click(object sender, RoutedEventArgs e)
        {
            TexboxEdicion(true);
            BotonesEnEdicionDeportes(true);
            LimpiarCampos();
        }

        private void btnEditarDepor_Click(object sender, RoutedEventArgs e)
        {
            TexboxEdicion(true);
            Deporte depor = dtgDeportes.SelectedItem as Deporte;
            if (depor != null)
            {
                txbNombreDepor.Text = depor.NombreDep;
                txbIdDeporte.Text = depor.Id.ToString();
                accionDeportes = accion.Editar;
                BotonesEnEdicionDeportes(true);
            }

        }

        private void btnGuardarDepor_Click(object sender, RoutedEventArgs e)
        {
            if (accionDeportes== accion.Nuevo)
            {
                if (string.IsNullOrEmpty(txbNombreDepor.Text))
                {
                    MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                else
                {
                    Deporte depor = new Deporte()
                    {
                        NombreDep = txbNombreDepor.Text
                    };
                    if (manejadorDeportes.Agregar(depor))
                    {
                        MessageBox.Show("Deporte generado correctamente", "Deportes", MessageBoxButton.OK, MessageBoxImage.Information);
                        LimpiarCampos();
                        ActualizarTablaDeportes();
                        BotonesEnEdicionDeportes(false);
                        TexboxEdicion(false);
                    }
                    else
                    {
                        MessageBox.Show("El Deporte no se pudo generar correctamente", "Deportes", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }

            }
            else
            {
                if (string.IsNullOrEmpty(txbNombreDepor.Text))
                {
                    MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                else
                {
                    Deporte depor = dtgDeportes.SelectedItem as Deporte;
                    depor.NombreDep = txbNombreDepor.Text;
                    if (manejadorDeportes.Modificar(depor))
                    {
                        MessageBox.Show("Deporte modificado correctamente", "Deportes", MessageBoxButton.OK, MessageBoxImage.Information);
                        LimpiarCampos();
                        ActualizarTablaDeportes();
                        BotonesEnEdicionDeportes(false);
                        TexboxEdicion(false);
                    }
                    else
                    {
                        MessageBox.Show("El Deporte no se pudo actualizar correctamente", "Deportes", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private void btnEliminarDepor_Click(object sender, RoutedEventArgs e)
        {
            Deporte deporte = dtgDeportes.SelectedItem as Deporte;
            if (deporte!=null)
            {
                if (MessageBox.Show("¿Realmente deseas eliminar este deporte?", "Depoertes", MessageBoxButton.YesNo, MessageBoxImage.Question)== MessageBoxResult.Yes)
                {
                    if (manejadorDeportes.Eliminar(deporte.Id))
                    {
                        MessageBox.Show("Deporte eliminado", "Deportes", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaDeportes();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el deporte", "Deportes", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnCancelarDepor_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCampos();
            BotonesEnEdicionDeportes(false);
            TexboxEdicion(false);
        }
    }
}
