﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Deportes.BIZ;
using Deportes.COMMON.Entidades;
using Deportes.COMMON.Interfaces;
using Deportes.DAL;

namespace Deportes.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para ventanaTorneo.xaml
    /// </summary>
    public partial class ventanaTorneo : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }
        IManejadorTorneo manejadorTorneo;
        IManejadorDeportes manejadorDeportes;
        IManejadorEquipos manejadorEquipos;
        IManejadorPartido manejadorPartido;
        List<Equipo> equipos;
        List<int> randoms;

        accion accionTorneo;
        public ventanaTorneo()
        {
            InitializeComponent();
            manejadorTorneo = new ManejadorTorneos(new RepositorioGenerico<Torneo>());
            manejadorEquipos = new ManejadorEquipo(new RepositorioGenerico<Equipo>());
            manejadorDeportes = new ManejadorDeporte(new RepositorioGenerico<Deporte>());
            manejadorPartido = new ManejadorPartido(new RepositorioGenerico<Partido>());

            equipos = new List<Equipo>();
            randoms = new List<int>();

            LimpiarCampos();
            BotonesEdicionTorneo(false);
            ActualizarTablaTorneo();
            ActualizarCombosDeportes();
            ActualizarCombosEquipos();
            TexboxEdicion(false);

        }

        private void ActualizarCombosEquipos()
        {
            cmbEquipoTorneo.ItemsSource = null;
            cmbEquipoTorneo.ItemsSource = manejadorEquipos.EquiposPorDeporte(cmbDeporteTorneo.Text);
        }

        private void ActualizarCombosDeportes()
        {
            cmbDeporteTorneo.ItemsSource = null;
            cmbDeporteTorneo.ItemsSource = manejadorDeportes.Listar;
        }

        private void TexboxEdicion(bool v)
        {
            txbNombreTorneo.IsEnabled = v;
            cmbEquipoTorneo.IsEnabled = v;
            cmbDeporteTorneo.IsEnabled = v;
        }

        private void ActualizarTablaTorneo()
        {
            dtgTorneo.ItemsSource = null;
            dtgTorneo.ItemsSource = manejadorTorneo.Listar;
        }

        private void BotonesEdicionTorneo(bool value)
        {
            btnCancelarTorneo.IsEnabled = value;
            btnEditarTorneo.IsEnabled = !value;
            btnEliminarTorneo.IsEnabled = !value;
            btnGuardarTorneo.IsEnabled = value;
            btnNuevoTorneo.IsEnabled = !value;
        }

        private void LimpiarCampos()
        {
            txbNombreTorneo.Clear();
            txbIdTorneo.Text = "";
            cmbDeporteTorneo.ItemsSource = null;
            cmbEquipoTorneo.ItemsSource = null;
        }


        private void CrearTorneo()
        {
            Random r = new Random();
            Random ran = new Random();

            if (!GenerarPartido(r.Next(0, equipos.Count - 1), ran.Next(0, equipos.Count - 1)))
            {
                CrearTorneo();
            }
        }
        private bool GenerarPartido(int uno, int dos)
        {
            if (randoms.Contains(uno) || randoms.Contains(dos))
            {
                return false;
            }
            else
            {
                manejadorPartido.Agregar(new Partido()
                {
                    EquipoLocal = equipos[uno],
                    EquipoVisitante = equipos[dos]
                });
                randoms.Add(uno);
                randoms.Add(dos);
                return true;
            }
        }


        private void btnNuevoTorneo_Click(object sender, RoutedEventArgs e)
        {
            TexboxEdicion(true);
            BotonesEdicionTorneo(true);
            LimpiarCampos();
            ActualizarCombosDeportes();
        }

        private void btnEditarTorneo_Click(object sender, RoutedEventArgs e)
        {
            Torneo torneo = dtgTorneo.SelectedItem as Torneo;
            if (torneo != null)
            {
                txbNombreTorneo.Text = torneo.NombreTorn;
                txbIdTorneo.Text = torneo.Id.ToString();
                accionTorneo = accion.Editar;
                BotonesEdicionTorneo(true);
            }
        }

        private void btnGuardarTorneo_Click(object sender, RoutedEventArgs e)
        {
            if (accionTorneo == accion.Nuevo)
            {
                Torneo torneo = new Torneo()
                {
                    NombreTorn = txbNombreTorneo.Text
                };
                if (manejadorTorneo.Agregar(torneo))
                {
                    MessageBox.Show("Torneo generado correctamente", "Torneos", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCampos();
                    ActualizarTablaTorneo();
                    BotonesEdicionTorneo(false);
                    TexboxEdicion(false);
                }
                else
                {
                    MessageBox.Show("El torneo no se pudo generar correctamente", "Torneos", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }
            else
            {
                Torneo torneo = dtgTorneo.SelectedItem as Torneo;
                torneo.NombreTorn = txbNombreTorneo.Text;
                if (manejadorTorneo.Modificar(torneo))
                {
                    MessageBox.Show("Torneo modificado correctamente", "Torneos", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCampos();
                    ActualizarTablaTorneo();
                    BotonesEdicionTorneo(false);
                    TexboxEdicion(false);
                }
                else
                {
                    MessageBox.Show("El torneo no se pudo actualizar correctamente", "Torneos", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnEliminarTorneo_Click(object sender, RoutedEventArgs e)
        {
            Torneo torneo = dtgTorneo.SelectedItem as Torneo;
            if (torneo != null)
            {
                if (MessageBox.Show("¿Realmente deseas eliminar este equipo?", "Torneos", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorTorneo.Eliminar(torneo.Id))
                    {
                        MessageBox.Show("Torneo eliminado", "Torneos", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaTorneo();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el torneo", "Torneos", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnCancelarTorneo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCampos();
            BotonesEdicionTorneo(false);
            TexboxEdicion(false);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnCrearTorneo_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnAgregarEquipoTorneo_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnEliminarEquipoTorneo_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
