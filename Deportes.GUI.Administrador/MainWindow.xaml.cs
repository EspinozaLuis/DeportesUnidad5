﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Deportes.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnAgregarDeporte_Click(object sender, RoutedEventArgs e)
        {
            VentanaDeporte ventanaDeporte = new VentanaDeporte();
            ventanaDeporte.Show();
        }

        private void btnAgregarEquipo_Click(object sender, RoutedEventArgs e)
        {
            ventanaEquipo ventanaEquip = new ventanaEquipo();
            ventanaEquip.Show();
        }

        private void btnCrearTorneo_Click(object sender, RoutedEventArgs e)
        {
            ventanaTorneo ventanaTorne = new ventanaTorneo();
            ventanaTorne.Show();
        }
    }
}
