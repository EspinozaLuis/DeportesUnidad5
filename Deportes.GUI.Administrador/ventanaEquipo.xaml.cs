﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Deportes.BIZ;
using Deportes.COMMON.Entidades;
using Deportes.COMMON.Interfaces;
using Deportes.DAL;

namespace Deportes.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para ventanaEquipo.xaml
    /// </summary>
    public partial class ventanaEquipo : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }
        
        IManejadorEquipos manejadorEquipos;
        IManejadorDeportes manejadorDeportes;
        
        accion accionEquipos;
        
        public ventanaEquipo()
        {
            InitializeComponent();
            manejadorEquipos = new ManejadorEquipo(new RepositorioGenerico<Equipo>());
            manejadorDeportes = new ManejadorDeporte(new RepositorioGenerico<Deporte>());
            LimpiarCampos();
            BotonesEnEdicionEquipos(false);
            ActualizarTablaEquipos();
            TexboxEdicion(false);
            ActualizarCombos();
        }

        private void ActualizarCombos()
        {
            cmbDeporteEquipo.ItemsSource = null;
            cmbDeporteEquipo.ItemsSource = manejadorDeportes.Listar;
        }

        private void TexboxEdicion(bool Value)
        {
            txbNombreEquipo.IsEnabled =Value;
            cmbDeporteEquipo.IsEnabled = Value;
        }

        private void ActualizarTablaEquipos()
        {
            dtgEquipos.ItemsSource = null;
            dtgEquipos.ItemsSource = manejadorEquipos.Listar;
        }

        private void BotonesEnEdicionEquipos(bool value)
        {
            btnCancelarEquipo.IsEnabled = value;
            btnEditarEquipo.IsEnabled = !value;
            btnEliminarEquipo.IsEnabled = !value;
            btnGuardarEquipo.IsEnabled = value;
            btnNuevoEquipo.IsEnabled = !value;
        }

        private void LimpiarCampos()
        {
            txbNombreEquipo.Clear();
            cmbDeporteEquipo.ItemsSource = null;
            txbIdEquipo.Text = "";
        }

        private void btnNuevoEquipo_Click(object sender, RoutedEventArgs e)
        {
            TexboxEdicion(true);
            BotonesEnEdicionEquipos(true);
            LimpiarCampos();
            ActualizarCombos();
        }

        private void btnEditarEquipo_Click(object sender, RoutedEventArgs e)
        {
            Equipo depor = dtgEquipos.SelectedItem as Equipo;
            if (depor != null)
            {
                txbNombreEquipo.Text = depor.NombreEqu;
                txbIdEquipo.Text = depor.Id.ToString();
                cmbDeporteEquipo.Text = depor.DeporteEqu;
                accionEquipos = accion.Editar;
                TexboxEdicion(true);
                BotonesEnEdicionEquipos(true);
            }
        }

        private void btnGuardarEquipo_Click(object sender, RoutedEventArgs e)
        {
            if (accionEquipos == accion.Nuevo)
            {
                
                if (string.IsNullOrEmpty(txbNombreEquipo.Text) || string.IsNullOrEmpty(cmbDeporteEquipo.Text))
                {
                    MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                else
                {
                    Equipo equipo = new Equipo()
                    {
                        NombreEqu = txbNombreEquipo.Text,
                        DeporteEqu= cmbDeporteEquipo.Text
                    };
                    if (manejadorEquipos.Agregar(equipo))
                    {
                        MessageBox.Show("Equipo generado correctamente", "Equipos", MessageBoxButton.OK, MessageBoxImage.Information);
                        LimpiarCampos();
                        ActualizarTablaEquipos();
                        BotonesEnEdicionEquipos(false);
                        TexboxEdicion(false);
                    }
                    else
                    {
                        MessageBox.Show("El equipo no se pudo generar correctamente", "Equipo", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }

            }
            else
            {
                if (string.IsNullOrEmpty(txbNombreEquipo.Text) || string.IsNullOrEmpty(cmbDeporteEquipo.Text))
                {
                    MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                else
                {
                    Equipo equipo = dtgEquipos.SelectedItem as Equipo;
                    equipo.NombreEqu = txbNombreEquipo.Text;
                    equipo.DeporteEqu = cmbDeporteEquipo.Text;
                    if (manejadorEquipos.Modificar(equipo))
                    {
                        MessageBox.Show("Equipo modificado correctamente", "Equipos", MessageBoxButton.OK, MessageBoxImage.Information);
                        LimpiarCampos();
                        ActualizarTablaEquipos();
                        BotonesEnEdicionEquipos(false);
                        TexboxEdicion(false);
                    }
                    else
                    {
                        MessageBox.Show("El Equipo no se pudo actualizar correctamente", "Equipos", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private void btnEliminarEquipo_Click(object sender, RoutedEventArgs e)
        {
            Equipo equipo = dtgEquipos.SelectedItem as Equipo;
            if (equipo != null)
            {
                if (MessageBox.Show("¿Realmente deseas eliminar este equipo?", "Equipos", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorEquipos.Eliminar(equipo.Id))
                    {
                        MessageBox.Show("Equipo eliminado", "Equipos", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaEquipos();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el equipo", "Equipos", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnCancelarEquipo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCampos();
            BotonesEnEdicionEquipos(false);
            TexboxEdicion(false);
        }
    }
}
