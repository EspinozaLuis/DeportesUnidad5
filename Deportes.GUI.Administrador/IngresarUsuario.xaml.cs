﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Deportes.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para IngresarUsuario.xaml
    /// </summary>
    public partial class IngresarUsuario : Window
    {
        private string usuario;
        private string contrasenia;

        public IngresarUsuario()
        {
            InitializeComponent();
            LimpiarCampos();
        }
        
        private void LimpiarCampos()
        {
            txbContrasenia.Clear();
            txbUsuario.Clear();
        }
        
        private void btnIngresar_Click(object sender, RoutedEventArgs e)
        {
            usuario = txbUsuario.Text;
            contrasenia = txbContrasenia.Text;
            if (string.IsNullOrEmpty(txbContrasenia.Text) || string.IsNullOrEmpty(txbUsuario.Text))
            {
                MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            else
            {
                if (usuario == "Administrador" && contrasenia == "predeterminada")
                {
                    MainWindow mainWindow = new MainWindow();
                    mainWindow.Show();
                }
                else
                {
                    MessageBox.Show("Usuario y/o Contraseña incorrecto", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCampos();
        }
    }
}
