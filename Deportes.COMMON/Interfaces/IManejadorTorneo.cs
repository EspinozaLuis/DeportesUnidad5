﻿using System;
using System.Collections.Generic;
using System.Text;
using Deportes.COMMON.Entidades;

namespace Deportes.COMMON.Interfaces
{
    public interface IManejadorTorneo:IManejadorGenerico<Torneo>
    {
        List<Torneo> TorneosPorDeporte(string torneo);
    }
}
