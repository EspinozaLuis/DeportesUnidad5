﻿using System;
using System.Collections.Generic;
using System.Text;
using Deportes.COMMON.Entidades;

namespace Deportes.COMMON.Interfaces
{
    public interface IManejadorEquipos:IManejadorGenerico<Equipo>
    {
        List<Equipo> EquiposPorDeporte(string equipo);
    }
}
