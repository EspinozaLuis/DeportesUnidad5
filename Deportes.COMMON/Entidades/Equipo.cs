﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Deportes.COMMON.Entidades
{
    public class Equipo:Base
    {
        public string NombreEqu { get; set; }
        public string DeporteEqu { get; set; }
        public override string ToString()
        {
            return string.Format("{0}", NombreEqu);
        }

        public static implicit operator string(Equipo v)
        {
            throw new NotImplementedException();
        }
    }
}
