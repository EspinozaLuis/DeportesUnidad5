﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Deportes.COMMON.Entidades
{
    public class Partido: Base
    {
        public string EquipoLocal { get; set; }
        public string EquipoVisitante { get; set; }
        public int MarcadorLocal { get; set; }
        public int MarcadorVisitante { get; set; }
    }
}
