﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Deportes.COMMON.Entidades
{
    public class Deporte : Base
    {
        public string NombreDep { get; set; }
        public override string ToString()
        {
            return string.Format("{0}", NombreDep);
        }
    }
   
}
